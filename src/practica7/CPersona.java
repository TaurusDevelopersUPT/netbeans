/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica7;

/**
 *
 * @author Usuario
 */
public class CPersona {
    String nombre;
    String edad;
    String sexo;
    String animal;
    String apodo;

    public CPersona(String nombre, String edad, String sexo, String animal, String apodo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.animal = animal;
        this.apodo = apodo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEdad() {
        return edad;
    }

    public String getSexo() {
        return sexo;
    }

    public String getAnimal() {
        return animal;
    }

    public String getApodo() {
        return apodo;
    }
    
}
